package me.JonaK.EV3RobotArm;

import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RemoteEV3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;

/**
 * @author Jonathan Kula
 * Created on 2/6/2016...
 * ...in project leJOS.
 * Copyright Jonathan Kula. All Rights Reserved.
 */


/**
 * Represents the Main Menu.
 */
public class MainMenu
{

    /**
     * Exits the Main Menu, ultimately closing the app (see {@link Main#program(RemoteEV3)}).
     * Mnemonic: 'q'
     */
    private JButton quitButton;

    /**
     * Opens the claw (motor A).
     * Mnemonic: 'o'
     */
    private JButton openButton;

    /**
     * Closes the claw (motor A).
     * Mnemonic: 'c'
     */
    private JButton closeButton;

    /**
     * Opens a {@link KeyMenu} with motor D.
     * Mnemonic: 'd'
     */
    private JButton descendButton;

    /**
     * Opens a {@link KeyMenu} with motor B.
     * Mnemonic: 'e'
     */
    private JButton extendButton;

    /**
     * Opens a {@link KeyMenu} with motor C.
     * Mnemonic: 'r'
     */
    private JButton rotateButton;

    /**
     * This JPanel contains everything else.
     */
    private JPanel Panel;

    /**
     * Opens a {@link KeyMenu} for ALL motors.
     */
    private JButton keyboardControlButton;

    /**
     * Statically defined to allow inner classes to close the window (see {@link #quitButton}).
     */
    private static JFrame frame;

    /**
     * Allows other methods to see if this menu exists. Default: false.
     */
    public static boolean exists = false;

    /**
     * Opens the menu.
     *
     * @param rmA Motor A
     * @param rmB Motor B
     * @param rmC Motor C
     * @param rmD Motor D
     */
    public static void startMenu(RMIRegulatedMotor rmA, RMIRegulatedMotor rmB, RMIRegulatedMotor rmC,
                                 RMIRegulatedMotor rmD)
    {
        exists = true; // The menu now exists!
        frame = new JFrame("MainMenu"); // Create the JFrame
        frame.setContentPane(new MainMenu(rmA, rmB, rmC, rmD).Panel); // Open the Main Menu, passing on all arguments.
        frame.setDefaultCloseOperation(
                JFrame.DISPOSE_ON_CLOSE); // Dispose the window, but don't terminate the program, on close.
        frame.pack();
        frame.setVisible(true); // Set the window to true.
    }

    /**
     * Constructs the Main Menu. Adds action listeners to all buttons.
     *
     * @param rmA Motor A
     * @param rmB Motor B
     * @param rmC Motor C
     * @param rmD Motor D
     */
    public MainMenu(final RMIRegulatedMotor rmA, final RMIRegulatedMotor rmB, final RMIRegulatedMotor rmC,
                    final RMIRegulatedMotor rmD)
    {

        quitButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                exists = false; // We're closing the window, so let's say it doesn't exist anymore.
                frame.dispatchEvent(
                        new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)); // Simulates closing the window.
            }
        });

        openButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                try {
                    Main.grabber(rmA, Direction.OPEN); // Open the claw (motor A)
                } catch (RemoteException e1) {
                    ErrorDialog.errorDialog(e1);
                }
            }
        });

        closeButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                try {
                    Main.grabber(rmA, Direction.CLOSE); // Close the claw (motor A)
                } catch (RemoteException e1) {
                    ErrorDialog.errorDialog(e1);
                }
            }
        });

        descendButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                KeyMenu.startKeyMenu(rmD); // Start a KeyMenu with motor D.
            }
        });

        extendButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                KeyMenu.startKeyMenu(rmB); // Start a KeyMenu with motor B.
            }
        });

        rotateButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                KeyMenu.startKeyMenu(rmC); // Start a KeyMenu with motor C.
            }
        });

        keyboardControlButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                KeyMenu.startKeyMenu(rmA, rmB, rmC, rmD);
            }
        });

        quitButton.setMnemonic('q'); // Set mnemonics for keyboard-only use (e.g. Steam Controller!)
        openButton.setMnemonic('o');
        closeButton.setMnemonic('c');
        descendButton.setMnemonic('d');
        extendButton.setMnemonic('e');
        rotateButton.setMnemonic('r');
        keyboardControlButton.setMnemonic('k');

    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$()
    {
        Panel = new JPanel();
        Panel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
        quitButton = new JButton();
        quitButton.setBackground(new Color(-12504775));
        quitButton.setHideActionText(true);
        quitButton.setText("Quit");
        Panel.add(quitButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 4,
                                                                               com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                               com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                               null, null, null, 0, false));
        openButton = new JButton();
        openButton.setText("Open");
        Panel.add(openButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 2,
                                                                               com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                               com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                               null, null, null, 0, false));
        keyboardControlButton = new JButton();
        keyboardControlButton.setBackground(new Color(-15299821));
        keyboardControlButton.setText("Keyboard Control");
        Panel.add(keyboardControlButton, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 4,
                                                                                          com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                          com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                                          com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                                          com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                                          com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                                          null, null, null, 0, false));
        rotateButton = new JButton();
        rotateButton.setText("Rotate");
        Panel.add(rotateButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                                 null, null, null, 0, false));
        extendButton = new JButton();
        extendButton.setText("Extend");
        Panel.add(extendButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 2,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                                 com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                                 null, null, null, 0, false));
        descendButton = new JButton();
        descendButton.setText("Descend");
        Panel.add(descendButton, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1,
                                                                                  com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                  com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                                  com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                                  com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                                  com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                                  null, null, null, 0, false));
        closeButton = new JButton();
        closeButton.setText("Close");
        Panel.add(closeButton, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 2,
                                                                                com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                                com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK |
                                                                                com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW,
                                                                                com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
                                                                                null, null, null, 0, false));
    }

    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$()
    {
        return Panel;
    }
}
