package me.JonaK.EV3RobotArm; /**
 * @author Jonathan Kula
 * Created on 2/6/2016...
 * ...in project leJOS.
 * Copyright Jonathan Kula. All Rights Reserved.
 */


/**
 * Defines the direction integers that represent opening or closing the claw.
 */
class Direction
{ // Utility Class

    /**
     * This integer represents opening the claw.
     */
    public static final int OPEN = 0;

    /**
     * This integer represents closing the claw.
     */
    public static final int CLOSE = 1;
}