package me.JonaK.EV3RobotArm;

import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RemoteEV3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;

/**
 * @author Jonathan Kula
 * Created on 2/6/2016...
 * ...in project leJOS.
 * Copyright Jonathan Kula. All Rights Reserved.
 */


/**
 * A special pop-up box that controls motors using the keyboard
 */
public class KeyMenu
{
    /**
     * Statically defined to allow inner classes to close the window (see {@link #quitButton}).
     */
    private static JFrame frame;

    public static boolean wPressed = false;
    public static boolean sPressed = false;
    public static boolean aPressed = false;
    public static boolean dPressed = false;
    public static boolean rPressed = false;
    public static boolean fPressed = false;

    /**
     * Allows other methods to see if this menu exists. Default: false.
     */
    public static boolean exists = false;

    /**
     * Opens the menu.
     *
     * @param rm Motor to control
     */
    public static void startKeyMenu(RMIRegulatedMotor rm)
    {
        exists = true;
        frame = new JFrame("KeyMenu");
        frame.setContentPane(new KeyMenu(rm).Panel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void startKeyMenu(RMIRegulatedMotor rmA, RMIRegulatedMotor rmB, RMIRegulatedMotor rmC,
                                    RMIRegulatedMotor rmD)
    {
        exists = true;
        frame = new JFrame("KeyMenu");
        try {
            frame.setContentPane(new KeyMenu(rmA, rmB, rmC, rmD).Panel);
        } catch (RemoteException e) {
            ErrorDialog.errorDialog(e);
        }
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Contains all elements of this GUI view
     */
    private JPanel Panel;

    /**
     * Exits the Main Menu, ultimately closing the app (see {@link Main#program(RemoteEV3)}).
     * Mnemonic: 'q'
     */
    private JButton quitButton;

    /**
     * The KeyListener is attached to this TextPane.
     */
    private JTextPane commandTextPane;

    /**
     * Initializes the KeyMenu
     *
     * @param rm Motor to control.
     */
    public KeyMenu(final RMIRegulatedMotor rm)
    {
        commandTextPane.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e) // Gets events when keys are pressed.
            {
                e.consume();
                if (e.getKeyCode() == KeyEvent.VK_W) { // Move backwards if 'W' is pressed.
                    wPressed = true; // Set this to true if 'W' is pressed.
                    try {
                        rm.forward(); // Move forward.
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_S) { // Move backwards if 'S' is pressed.
                    sPressed = true; // Set this to true if 'S' is pressed.
                    try {
                        rm.backward(); // Move backwards
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) // Gets events when keys are released.
            {
                e.consume();
                if (e.getKeyCode() == KeyEvent.VK_W) { // Indicate that 'W' was released...
                    wPressed = false; //...by setting this to false.
                }
                if (e.getKeyCode() == KeyEvent.VK_S) { // Indicate that 'S' was released...
                    sPressed = false; //...by setting this to false.
                }

                if (!wPressed && !sPressed) { // If neither are true...
                    try {
                        rm.stop(true); // Stop the motor (returning instantly).
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
            }
        });


        quitButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                exists = false; // We're closing the window, so let's say it doesn't exist anymore.
                frame.dispatchEvent(
                        new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)); // Simulates closing the window.
            }
        });

        quitButton.setMnemonic('q'); // Set a mnemonic for the quit button.
    }

    /**
     * This KeyMenu controls every motor (for controller support)
     * <p/>
     * 'W' and 'S' control extension. 'A' and 'D' control rotation. 'R' and 'F' control descent.
     * 'Q' and 'E' open and close the claw, respectively.
     *
     * @param motorA Claw Motor
     * @param motorB Extension Motor
     * @param motorC Rotation Motor
     * @param motorD Descent Motor
     */
    public KeyMenu(final RMIRegulatedMotor motorA, final RMIRegulatedMotor motorB, final RMIRegulatedMotor motorC,
                   final RMIRegulatedMotor motorD)
            throws RemoteException
    {

        motorC.setSpeed((int) motorC.getMaxSpeed());
        //motorC.setSpeed(50);

        commandTextPane.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e) // Gets events when keys are pressed.
            {
                e.consume();
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    wPressed = true;
                    try {
                        motorB.backward();
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_S) {
                    sPressed = true;
                    try {
                        motorB.forward(); // Move backwards
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }

                if (e.getKeyCode() == KeyEvent.VK_A) {
                    aPressed = true;
                    try {
                        motorC.forward();
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_D) {
                    dPressed = true;
                    try {
                        motorC.backward();
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }

                if (e.getKeyCode() == KeyEvent.VK_R) {
                    rPressed = true;
                    try {
                        motorD.backward();
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_F) {
                    fPressed = true;
                    try {
                        motorD.forward();
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }

                if (e.getKeyCode() == KeyEvent.VK_Q) {
                    try {
                        Main.grabber(motorA, Direction.OPEN);
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (e.getKeyCode() == KeyEvent.VK_E) {
                    try {
                        Main.grabber(motorA, Direction.CLOSE);
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) // Gets events when keys are released.
            {
                e.consume();
                if (e.getKeyCode() == KeyEvent.VK_W) { // Indicate that 'W' was released...
                    wPressed = false; //...by setting this to false.
                }
                if (e.getKeyCode() == KeyEvent.VK_S) { // Indicate that 'S' was released...
                    sPressed = false; //...by setting this to false.
                }

                if (e.getKeyCode() == KeyEvent.VK_A) { // And so on.
                    aPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_D) {
                    dPressed = false;
                }

                if (e.getKeyCode() == KeyEvent.VK_R) {
                    rPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_F) {
                    fPressed = false;
                }

                if (!wPressed && !sPressed) { // If neither are true...
                    try {
                        motorB.stop(true); // Stop the motor (returning instantly).
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (!aPressed && !dPressed) {
                    try {
                        motorC.stop(true);
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
                if (!rPressed && !fPressed) {
                    try {
                        motorD.stop(true);
                    } catch (RemoteException e1) {
                        ErrorDialog.errorDialog(e1);
                    }
                }
            }

        });


        quitButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                exists = false; // We're closing the window, so let's say it doesn't exist anymore.
                frame.dispatchEvent(
                        new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)); // Simulates closing the window.
            }
        });

        quitButton.setMnemonic('q'); // Set a mnemonic for the quit button.
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$()
    {
        Panel = new JPanel();
        Panel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        quitButton = new JButton();
        quitButton.setText("Quit");
        quitButton.setVerticalAlignment(0);
        Panel.add(quitButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1,
                                                                               com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                               com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW,
                                                                               com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW,
                                                                               null, null, null, 0, false));
        commandTextPane = new JTextPane();
        commandTextPane.setAutoscrolls(false);
        commandTextPane.setEditable(true);
        commandTextPane.setText("Command");
        Panel.add(commandTextPane, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1,
                                                                                    com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
                                                                                    com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH,
                                                                                    com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW,
                                                                                    com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW,
                                                                                    null, new Dimension(150, 50), null,
                                                                                    0, false));
    }

    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$()
    {
        return Panel;
    }
}
