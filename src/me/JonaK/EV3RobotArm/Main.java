package me.JonaK.EV3RobotArm;

import lejos.hardware.DeviceException;
import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RemoteEV3;

import javax.swing.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * @author Jonathan Kula
 * Created on 2/6/2016...
 * ...in project leJOS.
 * Copyright Jonathan Kula. All Rights Reserved.
 */


/**
 * Initialization Methods
 */
public class Main
{
    /**
     * This main method initializes the Remote EV3
     *
     * @param args Command line arguments (<code>args[0]</code> specifies an IP address)
     */
    public static void main(String[] args)
    {
        try { // Set look-and-feel
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (ClassNotFoundException e1) {
                ErrorDialog.errorDialog(e1);
            } catch (InstantiationException e1) {
                ErrorDialog.errorDialog(e1);
            } catch (IllegalAccessException e1) {
                ErrorDialog.errorDialog(e1);
            } catch (UnsupportedLookAndFeelException e1) {
                ErrorDialog.errorDialog(e1);
            }
        } catch (InstantiationException e) {
            ErrorDialog.errorDialog(e);
        } catch (IllegalAccessException e) {
            ErrorDialog.errorDialog(e);
        } catch (UnsupportedLookAndFeelException e) {
            ErrorDialog.errorDialog(e);
        }

        RemoteEV3 ev3; // Declare a Remote EV3

        String ip = "10.0.1.1";
        if (args.length == 1) {
            ip = args[0];
        }

        try {
            LoadingDialog.startLoad(ip); // Init the Remote EV3 at IP ip
            ev3 = new RemoteEV3(ip);
            LoadingDialog.finishLoad();
            program(ev3); // Start the program, which I aptly named "program," giving the remote EV3 as an argument.
        } catch (RemoteException e) {
            ErrorDialog.errorDialog(e);
        } catch (MalformedURLException e) {
            ErrorDialog.errorDialog(e);
        } catch (NotBoundException e) {
            ErrorDialog.errorDialog(e);
        } catch (DeviceException e) {
            ErrorDialog.errorDialog(e);
        }
    }

    /**
     * The main initialization of the program.
     * <p/>
     * Initializes motors A-D, starts the main menu, then closes all motors and plays a conformation tone.
     *
     * @param ev3 Remote EV3 Initialization.
     *
     * @throws RemoteException
     */
    private static void program(RemoteEV3 ev3)
            throws RemoteException, DeviceException
    {
        // Note -- RMI indicates a remotely managed "thing."
        RMIRegulatedMotor mA = ev3.createRegulatedMotor("A", 'M'); // Motor A is a MEDIUM EV3 motor.
        RMIRegulatedMotor mB = ev3.createRegulatedMotor("B", 'L'); // Motor B...
        RMIRegulatedMotor mC = ev3.createRegulatedMotor("C", 'L'); // ...and motor C...
        RMIRegulatedMotor mD = ev3.createRegulatedMotor("D", 'L'); // ...and motor D are all LARGE EV3 motors.

        MainMenu.startMenu(mA, mB, mC, mD); // Start the Main Menu, passing in the motors.
        while (MainMenu.exists) ; // Wait until the main menu exits

        mA.close(); // Close all motors
        mB.close();
        mC.close();
        mD.close();

        ev3.getAudio()
                .playTone(900, 500); // Play audio so we know we're all good.

        System.exit(0);

    }

    /**
     * Defines the motion of grabbing or releasing with the claw.
     * <p/>
     * Moves the <code>motor</code> backwards or forwards (according to <code>direction</code>)
     * until the motors stall.
     *
     * @param motor     The motor that represents the claw
     * @param direction Either 0 or 1, open or close. Values defined in {@link Direction}.
     *
     * @throws RemoteException
     */
    public static synchronized void grabber(RMIRegulatedMotor motor, int direction)
            throws RemoteException
    {
        motor.setSpeed((int) motor.getMaxSpeed()); // Set the motor speed to its max

        if (direction == Direction.OPEN) { // If the direction is OPEN, move the motor forward
            motor.forward();
        }
        else if (direction == Direction.CLOSE) {
            motor.backward(); // Else, backwards
        }

        while (!motor.isStalled()) ; // Keep going until the motor is stalled

        motor.stop(false); // Stop the motor and return.

    }

}